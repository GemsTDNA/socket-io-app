/**
 * Created by Radhika on 9/29/2017.
 */
var redis = require('redis');
var redisClient = redis.createClient(6379, '172.16.23.27');
var redisSubmit = redis.createClient(6379, '172.16.23.27');


function addLogMonitoringData(req, res, next) {
    logger.info("addLogMonitoring Function called:" + JSON.stringify(req.body))

    var fileName = req.body.fileName
    var path = req.body.path
    var watchDir = req.body.watchDirectory
    var pattern = req.body.pattern // array
    var IPAdress = req.body.IP
    var PRODUCT_NAME = req.body.product
    var VENDOR_NAME = req.body.vendor
    var LOCATION = req.body.location

    //add this to elastic search


    if (watchDir == true) { //is a directory , provide extension
        var extension = filename.split('.')[1]
        req.body.extension = extension
        var jsonObj = {
            //  "fileName": fileName,
            "path": path,
            "IP_ADDRESS": IPAdress,
            "pattern": req.body.pattern,
            "caseSensitive": req.body.caseSensitive,
            "TYPE": req.body.type,
            "watchDirectory": req.body.watchDirectory,
            "riseAlarm": req.body.riseAlarm,
            "extension": extension,
            "processing": 1,
            "PRODUCT_NAME": PRODUCT_NAME,
            "LOCATION": LOCATION,
            "VENDOR_NAME": VENDOR_NAME

        }

    }
    else {
        var jsonObj = {
            "fileName": fileName,
            "path": path,
            "IP_ADDRESS": IPAdress,
            "pattern": req.body.pattern,
            "caseSensitive": req.body.caseSensitive,
            "TYPE": req.body.type,
            "watchDirectory": req.body.watchDirectory,
            "riseAlarm": req.body.riseAlarm,
            "processing": 1,
            "PRODUCT_NAME": PRODUCT_NAME,
            "LOCATION": LOCATION,
            "VENDOR_NAME": VENDOR_NAME

        }
    }
    var arr = []
    var sessionid
    var stopLoop = false

    getNumberOfRecordsStored(function (errGettingNumberOfRecords, recordsFound) {
        if (errGettingNumberOfRecords) {
            logger.error(errGettingNumberOfRecords)
        }
        var jsonObjInEs = {
            "fileName": fileName,
            "path": path,
            "IP_ADDRESS": IPAdress,
            "pattern": req.body.pattern,
            "caseSensitive": req.body.caseSensitive,
            "TYPE": req.body.type,
            "watchDirectory": req.body.watchDirectory,
            "riseAlarm": req.body.riseAlarm,
            "processing": 1,
            "PRODUCT_NAME": PRODUCT_NAME,
            "LOCATION": LOCATION,
            "VENDOR_NAME": VENDOR_NAME

        }
        insertMonitoringData(jsonObjInEs, function (err, response) {
            if (err) {
                logger.error("error inserting data to elastic search:" + err)
            }
            var IDOfRecordInserted = response._id
            var IPOfRecordInserted = req.body.IP
            if (recordsFound.count == 0) {
                // use salt command to start  log Monitoring which is a nodejs process
                console.log('zero records found')
                console.log('use salt command to start  log Monitoring which is a nodejs process')
            }
            //check if key  exists in redis
            // yes - add or append or change status
            // no - create
            determineIfkeyExistsInRedis(IPOfRecordInserted, function (errWhileCheckingIfKeyExists, keyInformation) {
                if (errWhileCheckingIfKeyExists) {
                    logger.error(errWhileCheckingIfKeyExists)
                }
                logger.info("the key is:" + IPOfRecordInserted)
                logger.info("the key info is" + keyInformation)
                if (keyInformation == 0) {
                    //create redis keys
                    logger.info("redis hash with the key:" + IPOfRecordInserted + " " + "doesn't exist.")
                    res.send({"status": 1})
                    logger.info("creating redis hash:" + IPOfRecordInserted)
                    createRedisHash(IPOfRecordInserted, function (errWhileCreatingRedisHash, redisHashCreated) {
                        if (errWhileCreatingRedisHash) {
                            logger.error(errWhileCreatingRedisHash)
                            // return next(errWhileCreatingRedisHash)
                        }
                        logger.info("creating redis set:" + IPOfRecordInserted)
                        createRedisSet(IPOfRecordInserted, IDOfRecordInserted, function (errWhileCreatingRedisSet, redisSetCreated) {
                            if (errWhileCreatingRedisSet) {
                                logger.error(errWhileCreatingRedisSet)
                            }
                            logger.info(redisSetCreated)
                            //   res.send({"status": 1})
                        })
                    })
                }
                else {
                    res.send({"status": 1})
                    var stopLoop = false
                    var sessionid
                    // console.log("APPEND DATA TO REDIS SET")
                    // console.log(IPOfRecordInserted)
                    // console.log(IDOfRecordInserted)
                    createRedisSet(IPOfRecordInserted, IDOfRecordInserted, function (errWhileCreatingRedisSet, redisSetCreated) {
                        if (errWhileCreatingRedisSet) {
                            logger.error(errWhileCreatingRedisSet)
                        }

                        console.log("redisSetCreated for the IP:" + IPOfRecordInserted + " " + redisSetCreated)
                        fetchHashes(IPOfRecordInserted, function (errObtainingStatus, statusFound) {
                            if (errObtainingStatus) {
                                logger.error(errObtainingStatus)
                            }
                            if (statusFound == 'connected') {
                                logger.info("the status in the redis hash is connected")
                                IPAndSocketSession.forEach(function (eachObj) {
                                    if (stopLoop) {
                                        return
                                    }
                                    if (eachObj[IPOfRecordInserted]) {
                                        sessionid = eachObj[IPOfRecordInserted]
                                        stopLoop = true;
                                        //
                                    }
                                    jsonObj.id = response._id
                                    logger.info("emitting the following data to startProcessing event")
                                    logger.info(jsonObj)
                                    io.to(sessionid).emit("startProcessing", jsonObj)
                                    // res.send({"status": 1})
                                })


                            }
                            else {
                                logger.info("status is not connected:"+statusFound)
                            }
                        })

                    })

                }
            })

        })
    })

}


function createRedisHash(IP, callback) {
    redisSubmit.hset(IP, "status", "awaitingConnection", function (err, res) {
        if (err) {
            return callback(err)
        }
        //redisSubmit.expire(IP,300)
        return callback(null, res)
    });
}

function createRedisSet(IP, ID, callback) {
    logger.info("create redis set called with params " + IP + " " + ID)
    redisSubmit.SADD(IP + "_set", ID, function (err, res) {
        if (err) {
            return callback(err)
        }
        redisSubmit.expire(IP + "_set", 300)
        return callback(null, res)
    })
}

function determineIfkeyExistsInRedis(IP, callback) {

    redisSubmit.exists(IP, function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}


function fetchAllElementsOfset(IP, callback) {
    redisSubmit.smembers(IP + "_set", function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}

function fetchHashes(IP, callback) {
    redisSubmit.hget(IP, "status", function (err, res) {
        if (err) {
            return callback(err)
        }
        return callback(null, res)
    })
}


//function to check status and change it
//status void to connected1
// status connected1 to connected

function getNumberOfRecordsStored(callback) {
    elasticClient.count({
        index: "log_monitoring",
        type: "logs",

    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        return callback(null, response)
    })
}


function insertMonitoringData(data, callback) {
    elasticClient.index({
        index: "log_monitoring",
        type: "logs",
        body: data
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        return callback(null, response)
    })
}


function listData(req, res) {
    //list all data inserted into elastic search
    logger.info("listData function called")
    elasticClient.search({
        index: "log_monitoring",
        type: "logs",
        body: {
            size: 1000,
            query: {
                match_all: {}
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error(err)
        }

        var array = [];
        data.hits.hits.forEach(function (hit) {
            var jsonObj = hit['_source'];
            jsonObj['_id'] = hit['_id'];
            array.push(jsonObj);

        })
        res.json({"status": 1, "data": array});
    })


}

function editPattern(req, res) {
    // modify elastic search's pattern (replace)
    // api called
    // if status is connected or reconnected , then emit add Pattern data
    /* if no status found , then start salt process
     store update data in an data structure
     on connect => emit all the data

     */
    logger.info("saddPattern function called:" + JSON.stringify(req.body))
    var pattern = req.body.pattern //must be an array
    var id = req.body._id
    var IP = req.body.IP_ADDRESS
    var sessionid
    var stopLoop = false

    var jsonObj = {"pattern": pattern, "id": id, "IP": IP}

    logger.info("edit pattern api called with :" + JSON.stringify(req.body))


    var inlineScript = "ctx._source.pattern='" + pattern + "'";
    elasticClient.updateByQuery({
        index: "log_monitoring",
        type: "logs",
        body: {
            script: {
                inline: inlineScript,

            },
            query: {
                match: {
                    _id: req.body._id
                }
            }
        }
    }, function (err, response) {
        if (err) {
            logger.error(err)
        }
        //in production
        logger.info("the IPAndSocketSession array is shown below")
        logger.info(IPAndSocketSession)
        IPAndSocketSession.forEach(function (eachObj) {
            if (stopLoop) {
                return
            }
            if (eachObj[IP]) {
                sessionid = eachObj[IP]
                stopLoop = true;
                //
            }
        })


        logger.info("the client session id  obtained from IPandSocketSession array is " + sessionid)
        if (sessionid == undefined) {
            logger.info("no session ID found for IP" + IP + " " + "no workflow initiated")
        }

        workFlowForAddPattern(sessionid, IP, req.body._id, pattern, function (workFlowErr, workFlow) {
            if (workFlowErr) {
                return next(workFlowErr)
            }
        })


        // io.to(sessionid).emit("addPattern", jsonObj);


        res.send({"status": 1, "data": "success"})
    })

}


//used only for testing
function getIPAddress(_id, callback) {
    elasticClient.search({
        index: 'log_monitoring',
        type: 'logs',
        body: {
            size: 1000,
            query: {
                match: {_id: _id}
            }
        }
    }, function (err, data) {
        if (err) {
            logger.error("error querying elasticSearch" + JSON.stringify(err))
            return callback(err)
        }
        if (data) {
            logger.info("elastic search returns results")
            return callback(null, data.hits.hits[0]._source.IP_ADDRESS)
            // res.json({"status":1,"data":data.hits.hits})
        }
    })
}


function stopWatchingFile(req, res) {
    logger.info("stopWatchingFile function called:" + JSON.stringify(req.body))
    var id = req.body._id
    var IP = req.body.IP_ADDRESS
    var stopLoop = false
    var sessionid
    IPAndSocketSession.forEach(function (eachObj) {
        if (stopLoop) {
            return
        }
        if (eachObj[IP]) {
            sessionid = eachObj[IP]
            stopLoop = true;
            //
        }
    })
    logger.info("the IP is" + IP)
    logger.info("the session id is" + sessionid)
    //io.to(sessionid).emit("stopProcessing", req.body._id);
    workFlowForStopProcessing(sessionid, IP, req.body._id, function (workFlowErr, workFlowData) {
        if (workFlowErr) {
            return next(workFlowErr)
        }
    })
    updateProcessingFlag(id, function (err, data) {
        if (err) {
            logger.error(err)
        }

        res.json({"status": 1})
    })
}


function workFlowForStopProcessing(sessionid, IP, ID, callback) { //ID represents elastic search _id\
    getNumberOfRecordsStored(function (errGettingNumberOfRecords, recordsFound) {
        if (errGettingNumberOfRecords) {
            return callback(errGettingNumberOfRecords)
        }
        if (recordsFound.count == 0) {
            // use salt command to start  log Monitoring which is a nodejs process
            console.log('zero records found')
            console.log('use salt command to start  log-Monitoring which is a nodejs process')
        }

        determineIfkeyExistsInRedis(IP, function (errWhileCheckingIfKeyExists, keyInformation) {
            if (errWhileCheckingIfKeyExists) {
                return callback(errWhileCheckingIfKeyExists)
            }
            if (keyInformation) {
                //append
                createRedisSetForStopProcessing(IP, ID, function (err, data) {
                    if (err) {
                        logger.error(err)
                    }

                    fetchHashes(IP, function (errFetchingHash, hashData) {
                        if (errFetchingHash) {
                            return callback(errFetchingHash)
                        }
                        if (hashData == 'awaitingConnection') {
                            logger.info("status is awaitingConnection in stop Process  ")

                        }
                        if (hashData == 'connected') {
                            // emit immediately
                            logger.info("status is connected , hence emitting stopProcessing events")
                            io.to(sessionid).emit("stopProcessing", ID)
                            // determineIfkeyExistsInRedis(IP + "_stop_processing", function (errCheckingForRedisSet, keyFound) {
                            //
                            //     if (keyFound) {
                            //         // emit data stored in redis as well
                            //         fetchAllElementsOfset(IP + "_stop_processing", function (errFindingSetData, setDataFound) {
                            //             if (errFindingSetData) {
                            //                 logger.error(errFindingSetData)
                            //             }
                            //             logger.info("fetching data from redis set _stop_processing called:" + IP)
                            //             getDataBasedOnIDs(setDataFound, function (errFindingIDsInES, ESData) {
                            //                 if (errFindingIDsInES) {
                            //                     logger.error(errFindingIDsInES)
                            //                 }
                            //                 console.log(ESData)
                            //                 ESData.forEach(function (eachObj) {
                            //                     io.to(sessionid).emit("stopProcessing", eachObj)
                            //                     //socket.emit("startProcessing",eachObj);
                            //                     //o.to(sessionid).emit("addPattern", req.body);
                            //                 })
                            //
                            //
                            //             })
                            //         })
                            //
                            //     }
                            //     else {
                            //
                            //     }
                            // })

                        }
                    })
                })
            }
            else {
                //do nothing
                //and emit immediately
                logger.info("no such IP exists in redis:" + IP)
                logger.info("therefore creating redis set for stop processing for the IP:" + IP)
                createRedisHash(IP, function (errWhileCreatingRedisHash, redisHashCreated) {
                    if (errWhileCreatingRedisHash) {
                        logger.error(errWhileCreatingRedisHash)
                        // return next(errWhileCreatingRedisHash)
                    }
                    createRedisSetForStopProcessing(IP, ID, function (errCreatingRedisSet, setCreated) {
                        if (errCreatingRedisSet) {
                            return callback(errCreatingRedisSet)
                        }
                    })
                })
            }

        })
    })
}


function workFlowForAddPattern(sessionid, IP, ID, pattern) {
    getNumberOfRecordsStored(function (errGettingNumberOfRecords, recordsFound) {
        if (errGettingNumberOfRecords) {
            return callback(errGettingNumberOfRecords)
        }
        if (recordsFound.count == 0) {
            // use salt command to start  log Monitoring which is a nodejs process
            console.log('zero records found')
            console.log('use salt command to start  log-Monitoring which is a nodejs process')
        }
        logger.info("the count of records in elastic search is" + recordsFound.count)
        logger.info("checking to see if " + IP + " " + "exists in redis")
        determineIfkeyExistsInRedis(IP, function (errWhileCheckingIfKeyExists, keyInformation) {
            if (errWhileCheckingIfKeyExists) {
                return callback(errWhileCheckingIfKeyExists)
            }
            if (keyInformation) {
                logger.info("the IP exists in redis:" + IP)
                logger.info('appending to hash')
                createRedisHashForAddPattern(IP, ID, pattern, function (errCreatingRedisSet, setCreated) {
                    if (errCreatingRedisSet) {
                        return callback(errCreatingRedisSet)
                    }

                    fetchHashes(IP, function (errFetchingHash, hashData) {
                        if (errFetchingHash) {
                            return callback(errFetchingHash)
                        }
                        logger.info("the status for the given IP:" + IP + " " + "in redis is" + hashData)
                        if (hashData == 'awaitingConnection') {
                            console.log("status is awaitingConnection for add pattern event")
                        }
                        if (hashData == 'connected') {
                            // emit immediately
                            logger.info("the status is connected , hence emitting add pattern events")
                            io.to(sessionid).emit("addPattern", {"id": ID, "pattern": pattern})
                        }
                    })
                })
            }
            else {
                logger.info("no such IP exists in redis:" + IP)
                logger.info("therefore creating redis set for add pattern for the IP:" + IP)
                createRedisHash(IP, function (errWhileCreatingRedisHash, redisHashCreated) {
                    if (errWhileCreatingRedisHash) {
                        logger.error(errWhileCreatingRedisHash)
                        // return next(errWhileCreatingRedisHash)
                    }
                    createRedisHashForAddPattern(IP, ID, pattern, function (errCreatingRedisSet, setCreated) {
                        if (errCreatingRedisSet) {
                            return callback(errCreatingRedisSet)
                        }
                    })
                })

            }

        })
    })
}


function createRedisSetForStopProcessing(IP, ID, callback) {
    logger.info("create redis set for stop processing event called with params " + IP + " " + ID)
    redisSubmit.SADD(IP + "_stop_processing", ID, function (err, res) {
        if (err) {
            return callback(err)
        }
        redisSubmit.expire(IP + "_stop_processing", 300)
        return callback(null, res)
    })
}


function createRedisHashForAddPattern(IP, ID, pattern, callback) {
    logger.info("create redis set for add pattern event called with params " + IP + " " + ID)
    redisSubmit.HSET(IP + "_add_pattern", ID, pattern, function (err, res) {
        if (err) {
            return callback(err)
        }
        redisSubmit.expire(IP + "_add_pattern", 300)
        return callback(null, res)
    })
}

function updateProcessingFlag(id, callback) {
    var processing = 0
    var inlineScript = "ctx._source.processing='" + processing + "'";
    elasticClient.updateByQuery({
        index: "log_monitoring",
        type: "logs",
        body: {
            script: {
                inline: inlineScript,

            },
            query: {
                match: {
                    _id: id
                }
            }
        }
    }, function (err, response) {
        if (err) {
            return callback(err)
        }
        return callback(null, response)
    })
}


function getDataBasedOnIDs(listOfIDs, callback) {
    elasticClient.search({
        index: "log_monitoring",
        type: "logs",
        body: {
            query: {
                terms: {
                    "_id": listOfIDs
                }
            }
        }
    }, function (err, response) {
        if (err) {
            logger.error(err)
        }

        var results = []
        response.hits.hits.forEach(function (item) {
            var source = item._source
            source._id = item._id
            results.push(source)
        })
        return callback(null, results)
    })
}


function validateWhenAddingLogMonitoringData(req, res, next) {
    function isFilePathUnique(product, vendor, location, IP, pattern, callback) {
        elasticClient.count({
            index: "logs_monitoring",
            type: "logs",
            body: {
                query: {
                    bool: {
                        must: [
                            {term: {"PRODUCT_NAME": product}},
                            {term: {"VENDOR_NAME": vendor}},
                            {term: {"LOCATION": location}},
                            {term: {"IP": IP}},
                            {term: {"pattern": pattern}}


                        ]

                    }
                }
            }
        }, function (err, response) {
            if (err) {
                return callback(err)
            }
            console.log(response)
            if (response.count != 0) {
                return callback(null, {"status": 0, "data": "pattern already exists"})
            }
            else {
                return callback(null, {"status": 1, "data": "success"})
            }
        })
    }
}


function saltMinionCommand(callback) {
    var url = "https://10.225.253.133:9999/run";
    var headers = {
        'Accept': 'application/x-yaml',
        'X-Auth-Token': "41b9539436faae8016c305c2f875b31e47a23d93",
        'Content-type': 'application/json',
    };

    request({
        url: url,
        headers: headers,
        method: "POST",
        json: true,
        body: [{
            "client": "local",
            "tgt": IP,
            "fun": "cmd.run",
            "kwarg": {"cmd": 'cd /root/.GEMS/ && pm2 start audit.sh --name="GEMS-audit"', "stdin": "y"},
            "username": "salt",
            "password": "salt",
            "eauth": "pam"
        }]
    }, function (err, httpResponse, body) {
        if (err) {
            logger.info(err);
            //res.send("Failed");
        }

    })


}

exports.addLogMonitoringData = addLogMonitoringData
exports.editPattern = editPattern
exports.stopWatchingFile = stopWatchingFile
exports.listData = listData
exports.validateWhenAddingLogMonitoringData = validateWhenAddingLogMonitoringData