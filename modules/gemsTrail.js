var moment = require('moment');

exports.gemsTrailAssociation = function (req, res) {
	logger.silly("In the gemsTrailAssociation funtion"+ JSON.stringify(req.body));
	var jsonObj = req.body;
	elasticClient.index({
                index: 'gemstrail',
                type: 'association',
                body: jsonObj
        }, function (err, response) {
                if (err) {
                        logger.error("Error while inserting data into ES : " + err);
                } else {
                        jsonObj['_id'] = response['_id'];
                        io.to(jsonObj.PRODUCT_NAME).emit("gemstrail", jsonObj);
                        res.send(jsonObj);
                }
        });
}

exports.gemsTrailAnomoly = function (req, res) {
	logger.silly("In the gemsTrailAnomoly funtion"+ JSON.stringify(req.body));
	var key = Object.keys(req.body);
	var jsonObj = JSON.parse(key);
	jsonObj['PRODUCT_NAME'] = "SMSC";
	jsonObj.time = moment.parseZone(jsonObj.time).utc().format("YYYY-MM-DD HH:mm:ss");
	elasticClient.index({
		index: 'gemstrail',
                type: 'logs',
                body: jsonObj
	}, function (err, response) {
        	if (err) {
                	logger.error("Error while inserting data into ES : " + err);
                } else {
			jsonObj['_id'] = response['_id'];
			io.to(jsonObj.PRODUCT_NAME).emit("gemstrail", jsonObj);
			res.send(jsonObj);			
		}
	});
}

exports.gemsTrailRuleEngine = function (req, res) {
	var jsonObj = req.body;
	jsonObj['STATUS'] = "Workflow";
	logger.silly("In the gemsTrailRuleEngine function "+typeof(req.body));
	elasticClient.index({
                index: 'gemstrail',
                type: 'association',
                body: jsonObj
        }, function (err, response) {
                if (err) {
                        logger.error("Error while inserting data into ES : " + err);
                } else {
                        jsonObj['_id'] = response['_id'];
                        io.to(jsonObj.PRODUCT_NAME).emit("gemstrail", jsonObj);
                        res.send("Node receiced data correctly");
                }
        });
//	res.send("Ok");
}
