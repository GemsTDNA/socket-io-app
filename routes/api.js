var express = require('express');


var alerts = require("./alerts");

var router = express.Router();

router.use('/', alerts);

module.exports = router;
